from django.db import models


class Autor(models.Model):
    """Класс представляет автора модели."""

    name = models.CharField(max_length=100)
    bio = models.TextField()

class Category(models.Model):
    """Класс представляет категорию статьи."""

    name = models.CharField(max_length=40)

class Tag(models.Model):
    """Класс представляет тег, назначаемый статье."""

    name = models.CharField(max_length=20)

class Article(models.Model):
    """Класс представляет статью."""

    title = models.CharField(max_length=200)
    content = models.TextField()
    pub_date = models.DateTimeField()
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)
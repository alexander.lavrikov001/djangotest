from django.shortcuts import render
from django.views.generic import ListView
from .models import Article

class BasedView(ListView):
    """Класс для представления списка статей"""

    template_name = 'blogapp/article_list.html'
    context_object_name = 'articles'
    queryset = Article.objects.defer('content').select_related('category').prefetch_related('tags').all()
    #queryset = Article.objects.all()

    #print('Queryset: ', queryset[1])
from django.urls import path
from .views import hello_worls_view, GroupsListView

app_name='myapiapp'

urlpatterns=[
    path('hello/', hello_worls_view, name='hello'),
    path('groups/', GroupsListView.as_view(), name='groups')
]
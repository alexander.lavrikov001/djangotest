from rest_framework import serializers

from .models import Product, Order

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        depth=1
        fields=(
            'pk',
            'name',
            'description',
            'price',
            'discount',
            'created_at',
            'archivade',
            'created_by'
        )

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model=Order
        fields=(
            'delivery_adress',
            'promocode',
            'created_at',
            'user',
            'products',
            'recipt'
        )
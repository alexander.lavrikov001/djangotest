from typing import Sequence

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db import transaction

from ...models import Order, Product

class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Start demo bulc actions ")
        info=[
            ('Smartfone 1', 99),
            ('Smartfone 2', 199),
            ('Smartfone 3', 1199),
        ]
        products = [
            Product(name=name, price=price, created_by_id=1)
            for name, price in info
        ]
        result = Product.objects.bulk_create(products)

        for o in result:
            print(o)

        self.stdout.write("Done")
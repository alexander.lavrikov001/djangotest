from django.core.management import BaseCommand

from ...models import Product


class Command(BaseCommand):
    '''
    Creates products
    '''

    def handle(self, *args, **options):
        self.stdout.write('Start create products')

        product_names=[
            'Lptop',
            'Desctop',
            'Smartphone'
        ]

        for product_name in product_names:
            product,create=Product.objects.get_or_create(name=product_name)
            self.stdout.write(f'Created product {product_name}')

        self.stdout.write(self.style.SUCCESS("Product create"))
from typing import Sequence

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db import transaction
from django.db.models import Avg, Max, Min, Count, Sum

from ...models import Order, Product

class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Start demo agg ")
        # result = Product.objects.aggregate(
        #     Avg('price'),
        #     Max('price'),
        #     Min('price'),
        #     Count('id')
        # )
        # print(result)

        orders=Order.objects.annotate(
            total=Sum('products__price'),
            products_count=Count('products')
        )
        print(orders)
        self.stdout.write("Done")
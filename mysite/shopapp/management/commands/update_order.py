from django.core.management import BaseCommand

from ...models import Product, Order


class Command(BaseCommand):
    def handle(self, *args, **options):
        order=Order.objects.first()
        products=Product.objects.all()

        for product in products:
            order.products.add(product)

        order.save()
        self.stdout.write(f"Saved! {order.products.all()} to order {order}")
from typing import Sequence

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db import transaction

from ...models import Order, Product

class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Start demo select fields")
        product_values=Product.objects.values('pk', 'name')

        for p in product_values:
            print(p)
        self.stdout.write("Done")


from django.contrib.auth.models import User
from django.core.management import BaseCommand

from ...models import Order, Product


class Command(BaseCommand):
    def handle(self, *args, **options):
        for i in Product.objects.all():
            print(i)
        print(Product.objects.all())
"""
В этом модуле различные представления.

Разные представления для интернет-магазина:
по товарам, заказам и т.д.
"""

import logging
from csv import DictWriter
from timeit import default_timer

from django.contrib.auth.mixins import LoginRequiredMixin, \
    PermissionRequiredMixin, UserPassesTestMixin
from django.contrib.syndication.views import Feed
from django.core.cache import cache
from django.http import HttpRequest, HttpResponseRedirect, \
    JsonResponse, HttpResponse, HttpResponseNotFound
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.models import Group, User
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.cache import cache_page
from django.views.generic import ListView, DetailView, CreateView, \
    UpdateView, DeleteView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from rest_framework.request import Request
from rest_framework.response import Response

from .forms import OrdersForm, ProductForm, GroupForm
from .models import Product, Order, ProductImage
from .serializers import ProductSerializer, OrderSerializer
from .common import save_scv_products

from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter, OrderingFilter

from drf_spectacular.utils import extend_schema, OpenApiResponse

logger = logging.getLogger(__name__)


@extend_schema(description='Product views CRUD')
class ProductViewSet(ModelViewSet):
    """
    Набор представлений для действий над Product.

    Полный CRUD для сущности товара
    """

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [
        SearchFilter,
        DjangoFilterBackend,
        OrderingFilter
    ]
    search_fields = ['name', 'description']
    filterset_fields = [
        'name',
        'description',
        'price'
    ]
    ordering_fields = [
        'name',
        'description',
        'price'
    ]

    @extend_schema(
        summary='Get one product by ID',
        description='Retrives product, returns 404',
        responses={
            200: ProductSerializer,
            404: OpenApiResponse(
                description='Empty responce, product by id not found')
        }
    )
    def retrieve(self, *args, **kwargs):
        """Метод расширения документации."""
        return super().retrieve(*args, **kwargs)

    @action(methods=['get'], detail=False)
    def download_csv(self, reuest: Request):
        response = HttpResponse(content_type='text/csv')
        filename = 'products-export.csv'
        response['Content-Disposition'] = f'attachment; filename={filename}'
        queryset = self.filter_queryset(self.get_queryset())
        fields = [
            'name',
            'description',
            'price'
        ]
        queryset = queryset.only(*fields)
        writer = DictWriter(response, fieldnames=fields)
        writer.writeheader()

        for product in queryset:
            writer.writerow({
                field: getattr(product, field)
                for field in fields
            })
        return response

    @action(methods=['post'], detail=False, parser_classes=[MultiPartParser])
    def upload_csv(self, request: Request):
        products = save_scv_products(
            request.FILES['file'].file,
            encoding=request.encoding
        )
        serializer = self.get_serializer(products, many=True)
        return Response(serializer.data)

    @method_decorator(cache_page(60))
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)


class OrderViewSet(ModelViewSet):
    """Представление множества заказов."""

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = [
        SearchFilter,
        DjangoFilterBackend,
        OrderingFilter
    ]
    serch_fields = ['delivery_adress', 'user']
    filterset_fields = [
        'delivery_adress',
        'promocode',
        'created_at',
        'user'
    ]
    ordering_fields = [
        'delivery_adress',
        'promocode',
        'created_at',
        'user',
        'products'
    ]


class ShopIndexView(View):
    """Представление индекса. Тестовая страница."""

    def get(self, request: HttpRequest):
        """Метод обработки GET-запроса."""
        products = [
            ('Laptop', 1999),
            ('Desctop', 199999),
            ('Smartphone', 19999),
        ]
        context = {
            'timer': default_timer(),
            'products': products
        }
        logging.debug("Products for shop index: %s", products)
        logging.info('Rendering shop index')
        print('shop index context', context)
        return render(request, 'shopapp/shop-index.html', context=context)


class GroupsListView(View):
    """Представление списка групп."""

    def get(self, request: HttpRequest):
        """Метод обработки GET-запроса."""
        context = {
            'form': GroupForm(),
            'groups': Group.objects.all(),
        }
        return render(request, 'shopapp/droups-list.html', context=context)

    def post(self, request: HttpRequest):
        """Метод обработки POST-запроса."""
        form = GroupForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect(request.path)


class ProductDetailsView(DetailView):
    """Представление деталей продукта."""

    template_name = 'shopapp/product-details.html'
    # model = Product
    queryset = Product.objects.prefetch_related('images')
    context_object_name = 'product'


# class ProductDetailsView(View):
#     def get(self, request:HttpRequest, pk: int):
#         product=get_object_or_404(Product, pk=pk)
#         context={
#             'product': product,
#         }
#         return render(
#                       request,
#                       'shopapp/product-details.html',
#                       context=context
#                       )


class ProductsListView(ListView):
    """Представление списка продуктов."""

    template_name = 'shopapp/products-list.html'
    # model=Product
    context_object_name = 'products'
    queryset = Product.objects.filter(archivade=False)


# class ProductsListView(TemplateView):
#     template_name='shopapp/products-list.html'
#
#     def get_context_data(self, **kwargs):
#         context=super().get_context_data(**kwargs)
#         context['products']=Product.objects.all()
#         return context

# def products_list(request: HttpRequest):
#     context={
#         'products': Product.objects.all(),
#     }
#     return render(request, 'shopapp/products-list.html', context=context)

# def orders_list(request: HttpRequest):
#     context={
#         'orders':
#         Order.objects.select_related('user').prefetch_related("products").all(),
#     }
#     return render(request, 'shopapp/order_list.html', context=context)

class OrdersListView(LoginRequiredMixin, ListView):
    """Представление для списка заказов."""

    queryset = (
        Order.objects.select_related('user').prefetch_related('products')
    )


class OrderDetailView(PermissionRequiredMixin, DetailView):
    """Представление для деталей заказа."""

    permission_required = 'shopapp.view_order'
    queryset = (
        Order.objects.select_related('user').prefetch_related('products')
    )


class OrderUpdateView(UpdateView):
    """Представление для обновления заказа."""

    model = Order
    fields = 'delivery_adress', 'promocode', 'user', 'products'
    template_name_suffix = '_update_form'

    def get_success_url(self):
        """Метод перенаправления ссылки."""
        return reverse('shopapp:order_detail', kwargs={'pk': self.object.pk})


class OrderDeleteView(DeleteView):
    """Представление для удаления заказа."""

    model = Order
    success_url = reverse_lazy('shopapp:orders')


class ProductCreateView(PermissionRequiredMixin, CreateView):
    """Представление для создания продукта."""

    permission_required = 'shopapp.add_product'
    model = Product
    fields = 'name', 'price', 'description', 'discount', 'preview'

    success_url = reverse_lazy('shopapp:products')

    def form_valid(self, form):
        """Метод проверки доступа."""
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class ProductUpdateView(UserPassesTestMixin, UpdateView):
    """Представление для обновления продукта."""

    def test_func(self):
        """Метод проверки доступа."""
        if self.request.user.is_superuser:
            return True
        self.object = self.get_object()
        has_edit_perm = self.request.user.has_perm("shopapp.change_product")
        created_by_current_user = self.object.created_by == self.request.user
        return has_edit_perm and created_by_current_user

    model = Product
    # fields = 'name', 'price', 'description', 'discount', 'preview'
    form_class = ProductForm
    form_class = ProductForm
    template_name_suffix = '_update_form'

    def form_valid(self, form):
        """Проверка валидации."""
        response = super().form_valid(form)
        for image in form.files.getlist('images'):
            ProductImage.objects.create(
                product=self.object,
                image=image
            )
        return response

    def get_success_url(self):
        """Метод перенаправления ссылки."""
        return reverse(
            'shopapp:products_details',
            kwargs={'pk': self.object.pk})


class ProductDeleteView(DeleteView):
    """Представление удаления продукта."""

    # model = Product
    queryset = Product.objects.prefetch_related('images')
    success_url = reverse_lazy('shopapp:products')

    def form_valid(self, form):
        """Проверка валидации."""
        success_url = self.get_success_url()
        self.object.archivade = True
        self.object.save()
        return HttpResponseRedirect(success_url)


def create_product(request: HttpRequest):
    """Метод создания продукта."""
    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            # Product.objects.create(**form.cleaned_data)
            form.save()
            url = reverse('shopapp:products')
            return redirect(url)
    else:
        form = ProductForm()
    context = {
        'form': form
    }

    return render(request, 'shopapp/create-product.html', context=context)


def create_order(request: HttpRequest):
    """Метод создания заказа."""
    if request.method == 'POST':
        form = OrdersForm(request.POST)
        form.instance.user = request.user
        if form.is_valid():
            form.save()
            url = reverse('shopapp:orders')
            return redirect(url)
    else:
        form = OrdersForm()
    context = {
        'form': form
    }
    return render(request, 'shopapp/create-orders.html', context=context)


class ProductsDataExportView(View):
    """Метод для экспорта представления данных продукта."""

    def get(self, request: HttpRequest):
        """Метод обработки GET-запроса."""
        cache_key = "products_data_export"
        products_data = cache.get(cache_key)
        if products_data is None:
            products = Product.objects.order_by('pk').all()
            products_data = [
                {
                    'pk': product.pk,
                    'name': product.name,
                    'price': product.price
                }
                for product in products
            ]
            cache.set(cache_key, products_data, 300)
        elem = products_data[0]
        name = elem['name']
        print('name:', name)
        return JsonResponse({'products': products_data})


class OrdersDataExportView(UserPassesTestMixin, View):
    """Класс для экспорта представления данных заказа."""

    def test_func(self):
        """Метод для проверки разрешений."""
        return self.request.user.has_perm('is_staff')

    def get(self, request: HttpRequest):
        """Метод обработки GET-запроса."""
        orders = Order.objects.order_by('pk').all()
        orders_data = [
            {
                'pk': order.pk,
                'adress': order.delivery_adress,
                'promocode': order.promocode,
                'user': order.user.pk,
                'products': [
                    {
                        'pk': product.pk,
                        'name': product.name,
                        'price': product.price
                    }
                    for product in order.products.all()
                ]
            }
            for order in orders
        ]
        return JsonResponse({'orders': orders_data})


class LatestArticlesFeed(Feed):
    title = 'Каталог товаров'
    description = 'Обновление и изменение товаров'
    link = reverse_lazy('shopapp:products')

    def items(self):
        return Product.objects.filter(archivade__isnull=False).order_by('-created_at')[:5]

    def item_title(self, item: Product):
        return item.name

    def item_description(self, item: Product):
        return item.description[:100]


class UserOrdersView(View):

    def get(self, request: HttpRequest, pk):

        if not request.user.is_authenticated:
            return redirect(reverse('myauth:login'))
        try:
            user = User.objects.get(pk=pk)
        except:
            return HttpResponseNotFound("Такого пользователя нет")

        context = {
            'user': user,
            'orders': list(Order.objects.filter(user=user))
        }
        print(context['orders'])
        return render(request=request, template_name='shopapp/user-orders.html', context=context)


class UserOrderExportView(View):
    def get(self, request: HttpRequest, pk):

        if not request.user.is_authenticated:
            return redirect(reverse('myauth:login'))
        try:
            user = User.objects.get(pk=pk)
        except:
            return HttpResponseNotFound("Такого пользователя нет")

        orders_data = cache.get("orders_data")
        if orders_data in None:

            orders = Order.objects.filter(user=user).order_by('pk')
            orders_data = [
                {
                    'pk': order.pk,
                    'adress': order.delivery_adress,
                    'promocode': order.promocode,
                    'user': order.user.pk,
                    'products': [
                        {
                            'pk': product.pk,
                            'name': product.name,
                            'price': product.price
                        }
                        for product in order.products.all()
                    ]
                }
                for order in orders
            ]

            cache.set('orders_data', orders_data, 300)

        return JsonResponse({'orders': orders_data})

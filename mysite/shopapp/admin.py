from django.contrib import admin
from django.db.models import QuerySet
from django.http import HttpRequest
from django.shortcuts import render, redirect
from django.urls import path

from .admin_mixins import ExportAsCSVMixin
from .common import save_scv_products
from .models import Product, Order, ProductImage
from .forms import CSVImportForm


@admin.action(description='Archivade product')
def mark_archivade(modeladmin: admin.ModelAdmin, request: HttpRequest, queryset: QuerySet):
    queryset.update(archivade=True)


@admin.action(description='Unarchivade product')
def mark_unarchivade(modeladmin: admin.ModelAdmin, request: HttpRequest, queryset: QuerySet):
    queryset.update(archivade=False)


class OrderInline(admin.TabularInline):
    model = Product.orders.through


class ProductInline(admin.StackedInline):
    model = ProductImage


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin, ExportAsCSVMixin):
    change_list_template = 'shopapp/products_change_list.html'
    actions = [
        mark_archivade,
        mark_unarchivade,
        'export_as_csv'
    ]
    inlines = [
        OrderInline,
        ProductInline
    ]
    list_display = 'pk', 'name', 'price', 'discount', 'description', 'archivade'
    list_display_links = 'pk', 'name'
    ordering = 'pk',
    search_fields = 'name', 'description'
    fieldsets = [
        (None, {
            'fields': ('name', 'description'),
        }),
        ('Price', {
            'fields': ('price', 'discount'),
            'classes': ('collapse',)
        }),
        ('Images', {
            'fields': ('preview',)
        }),
        ('Extra', {
            'fields': ('archivade',)
        })
    ]

    def import_csv(self, request: HttpRequest):
        if request.method=="GET":
            form = CSVImportForm()
            context = {
                'form': form
            }
            return render(request, 'admin/csv_form.html', context=context)
        form= CSVImportForm(request.POST, request.FILES)
        if not form.is_valid():
            context={
                "form": form
            }
            return render(request, 'admin/csv_form.html', context=context, status=400)
        save_scv_products(
            file=form.files['csv_file'].file,
            encoding=request.encoding
        )

        self.message_user(request, "Data from CSV was imported")
        return redirect('..')


    def get_urls(self):
        urls = super().get_urls()
        new_urls = [
            path(
                "import-products-csv/",
                self.import_csv,
                name='import_products_csv'
            )
        ]
        return new_urls + urls

    def description_short(self, obj: Product):
        if len(obj.description) < 48:
            return obj.description
        return obj.description[:48] + '...'


# admin.site.register(Product, ProductAdmin)


class ProductInline(admin.StackedInline):
    model = Order.products.through


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    change_list_template = 'shopapp/orders_change_list.html'
    inlines = [
        ProductInline
    ]
    list_display = 'delivery_adress', 'promocode', 'created_at', 'user_verbose'

    def get_queryset(self, request):
        return Order.objects.select_related('user').prefetch_related('products')

    def user_verbose(self, obj: Order):
        return obj.user.first_name or obj.user.username

    def import_csv(self, request: HttpRequest):
        form=CSVImportForm()
        context ={
            'form': form
        }
        return render(request, 'admin/csv_form.html', context)

    def get_urls(self):
        urls = super().get_urls()
        new_urls = [
            path(
                "import-orders-csv/",
                self.import_csv,
                name='import_orders_csv'
            )
        ]
        return new_urls + urls
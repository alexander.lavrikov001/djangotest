import json
import random

from django.conf import settings
from django.contrib.admin import ModelAdmin
from django.contrib.auth.models import User, Permission, PermissionsMixin
from django.test import TestCase
from django.urls import reverse

from .models import Product, Order
from .utils import add


# Create your tests here.
class AddTestCase(TestCase):
    def test_add(self):
        res = add(2, 3)
        self.assertEquals(res, 5)


class ProductCreateViewTestCase(TestCase):
    def test_create_product(self):
        responce = self.client.post(reverse("shopapp:product_create"),
                                    {
                                        'name': 'Keyboard', 'price': '999.88', 'description': 'Keyboard for PK',
                                        'discount': '0'
                                    })
        self.assertRedirects(responce, reverse('shopapp:products'))


class ProductDetailsViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.product = Product.objects.create(name='speaker')

    # def setUp(self) -> None:
    #     self.product=Product.objects.create(name='speaker')

    def test_product_detail_view(self):
        responce = self.client.get(reverse('shopapp:products_details', kwargs={'pk': self.product.pk}))
        self.assertEquals(responce.status_code, 200)

    def test_product_detail_view_and_check_content(self):
        responce = self.client.get(reverse('shopapp:products_details', kwargs={'pk': self.product.pk}))
        self.assertContains(responce, self.product.name)

    @classmethod
    def tearDownClass(cls):
        cls.product.delete()

    # def tearDown(self) -> None:
    #     self.product.delete()


class ProductListViewTestCase(TestCase):
    fixtures = [
        'product-fixtures.json'
    ]

    def test_products(self):
        responce = self.client.get(reverse('shopapp:products'))
        # for product in Product.objects.filter(archived=False).all():
        #     self.assertContains(responce, product.name)
        products = Product.objects.filter(archived=False).all()
        products_ = responce.context['products']
        # for p, p_ in zip(products, products_):
        #     self.assertEquals(p.pk, p_.pk)
        self.assertQuerySetEqual(
            qs=Product.objects.filter(archived=False).all(),
            values=(p.pk for p in responce.context['products']),
            transform=lambda p: p.pk
        )
        self.assertTemplateUsed(responce, 'shopapp/products-list.html')


class OrderListViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.credentials = dict(username='test', password='test_password4')
        cls.user = User.objects.create_user(**cls.credentials)

    def setUp(self) -> None:
        self.client.login(**self.credentials)

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def test_orders_view(self):
        response = self.client.get(reverse('shopapp:orders'))
        self.assertContains(response, 'Orders')

    def test_not_view_authenticated(self):
        self.client.logout()
        respons = self.client.get(reverse('shopapp:orders'))
        self.assertEquals(respons.status_code, 302)
        self.assertIn(str(settings.LOGIN_URL), respons.url)


class ProductExportViewTestCase(TestCase):
    fixtures = [
        'product-fixtures.json',
        'user-fixtures.json'
    ]

    def test_get_products_view(self):
        response = self.client.get(reverse('shopapp:products-export'))
        self.assertEquals(response.status_code, 200)
        products = Product.objects.order_by('pk').all()
        expected_data = [
            {
                'pk': product.pk,
                'name': product.name,
                'price': str(product.price)

            }
            for product in products
        ]

        products_data = response.json()
        self.assertEquals(
            products_data['products'],
            expected_data,
        )


class OrderDetailViewTestCase(TestCase):
    fixtures = ['product-fixtures.json',
                'user-fixtures.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.credentials = dict(username='test', password='test_password4')
        cls.user = User.objects.create_user(**cls.credentials)
        cls.user.user_permissions.set([25, 26, 32])

    def setUp(self) -> None:
        self.client.login(**self.credentials)

        self.prod = Product.objects.all()[random.randint(0, len(Product.objects.all()))]

        response = self.client.post(reverse("shopapp:create_order"),
                                    {
                                        'delivery_adress': 'Adress for delivery',
                                        'promocode': 'some promo',
                                        'products': [self.prod.pk]
                                    })
        self.assertEquals(Order.objects.count(), 1)

    def test_order_detail(self):
        print('start test')
        pk = Order.objects.all()[len(Order.objects.all()) - 1].pk
        response = self.client.get(reverse('shopapp:order_detail', kwargs={'pk': pk}))
        content = str(response.content)
        if content.count('<p>PK: ') == 1:
            pk_in_response = content[content.find('<p>PK: ') + 7]
            self.assertEquals(pk, int(pk_in_response))
        else:
            raise ValueError('Error in response. A more thorough study is needed.')
        self.assertContains(response, 'Adress for delivery')
        self.assertContains(response, 'some promo')

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def tearDown(self) -> None:
        response = self.client.post(reverse("shopapp:products_delete", kwargs={'pk': self.prod.pk}))


class OrdersExportTestCase(TestCase):
    fixtures = ['product-fixtures.json',
                'user-fixtures.json',
                'order-fixtures.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.credentials = dict(username='supertest', password='supertest_password9')
        cls.user_supertest=User.objects.create_user(**cls.credentials, is_staff=True, is_superuser=True)

    def setUp(self) -> None:

        self.client.login(**self.credentials)


    def test_orders_json(self):
        response = self.client.get(reverse('shopapp:orders-export'))
        self.assertEquals(response.status_code, 200)

        orders_data = [
            {
                'pk': order.pk,
                'adress': order.delivery_adress,
                'promocode': order.promocode,
                'user': order.user.pk,
                'products': [
                    {
                        'pk': product.pk,
                        'name': product.name,
                        'price': str(product.price)
                    }
                    for product in order.products.all()
                ]
            }
            for order in Order.objects.order_by('pk').all()
        ]


        self.assertEquals(response.json(), {'orders': orders_data})
    @classmethod
    def tearDownClass(cls):
        cls.user_supertest.delete()


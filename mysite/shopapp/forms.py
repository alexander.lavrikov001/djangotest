from django.forms import ModelForm
from django.contrib.auth.models import Group
from django import forms
from django.core import validators
from .models import Product, Order


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = 'name',


# class ProductForm(forms.Form):
#     name=forms.CharField(max_length=50)
#     price=forms.DecimalField(min_value=0, decimal_places=2)
#     description=forms.CharField(widget=forms.Textarea,
#                                 validators=[validators.RegexValidator(regex='great', message='Using word "great"')],
#                                 )


class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True


class MultipleFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = single_file_clean(data, initial)
        return result


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = 'name', 'price', 'description', 'discount', 'preview'

    images = MultipleFileField()


class OrdersForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = 'delivery_adress', 'promocode', 'products'


class CSVImportForm(forms.Form):
    csv_file=forms.FileField()
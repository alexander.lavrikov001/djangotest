from django.contrib.sitemaps import Sitemap

from .models import Product

class NewsSitemap(Sitemap):
    changefreq='always'
    priority=0.75
    def items(self):
        return Product.objects.filter(archivade__isnull=False).order_by('-created_at')

    def lastmod(self, obj:Product):
        return obj.published_at
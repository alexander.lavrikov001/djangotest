from django.contrib import admin
from django.urls import path, include
from django.views.decorators.cache import cache_page
from rest_framework.routers import DefaultRouter
from .views import ShopIndexView, GroupsListView, \
    OrdersListView, create_product, create_order, ProductDetailsView, ProductsListView,\
    OrderDetailView, ProductCreateView, ProductUpdateView, ProductDeleteView, OrderUpdateView, \
    OrderDeleteView, ProductsDataExportView, OrdersDataExportView, ProductViewSet, OrderViewSet, \
    LatestArticlesFeed, UserOrdersView, UserOrderExportView

app_name='shopapp'

routers=DefaultRouter()
routers.register('products', ProductViewSet)
routers.register('orders', OrderViewSet)

urlpatterns = [
    path('', cache_page(120)(ShopIndexView.as_view()), name='index'),
    path('groups/',GroupsListView.as_view(), name='groups'),
    path('products/', ProductsListView.as_view(), name='products'),
    path('products/create/', ProductCreateView.as_view(), name='product_create'),
    path('products/<int:pk>', ProductDetailsView.as_view(), name='products_details'),
    path('products/<int:pk>/update', ProductUpdateView.as_view(), name='products_update'),
    path('products/<int:pk>/delete', ProductDeleteView.as_view(), name='products_delete'),
    path('products/latest/feed/', LatestArticlesFeed(), name='products_lasted_feed'),
    path('products/export', ProductsDataExportView.as_view(), name='products-export'),
    path('orders/', OrdersListView.as_view(), name='orders'),
    path('orders/<int:pk>', OrderDetailView.as_view(), name='order_detail'),
    path('orders/<int:pk>/update', OrderUpdateView.as_view(), name='order_update'),
    path('orders/<int:pk>/delete', OrderDeleteView.as_view(), name='order_delete'),
    path('create-product/', create_product, name='create_product'),
    path('create-order/', create_order,name='create_order'),
    path('orders/export', OrdersDataExportView.as_view(), name='orders-export'),
    path('api/', include(routers.urls)),
    path('users/<int:pk>/orders/', UserOrdersView.as_view(), name='user-orders'),
    path('users/<int:pk>/orders/export/', UserOrderExportView.as_view(), name='user-orders-export'),



]

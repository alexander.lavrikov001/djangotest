from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy


def product_preview_directory_path(instanse: "Product", filename: str) -> str:
    return f'products/product_{instanse.pk}/preview/{filename}'

def product_images_directory_path(instanse: "ProductImage", filename: str) -> str:
    return f'products/product_{instanse.product.pk}/images/{filename}'




class Product(models.Model):
    """
    Модель Product представляет товар, который можно продавать в интернет-магазине.

    Заказы тут: :model:`shopapp.Order`
    """
    class Meta:
        ordering = ['name']
        verbose_name = gettext_lazy('Product')
        verbose_name_plural = gettext_lazy('Products')

    name = models.CharField(max_length=100, db_index=True)
    description = models.TextField(null=False, blank=True, db_index=True)
    price = models.DecimalField(default=0, max_digits=15, decimal_places=2)
    discount = models.SmallIntegerField(default=0)
    created_at = models.DateField(auto_now_add=True)
    archivade = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT)
    preview = models.ImageField(null=True, blank=True, upload_to=product_preview_directory_path)

    # @property
    # def descriprion_short(self):
    #     if(len(self.description))<48:
    #         return self.description
    #     return self.description[:48]+'...'

    def get_absolute_url(self):
        return reverse('shopapp:products_details', kwargs={'pk':self.pk})

    def __str__(self):
        return f'{self.name}: {self.price}'


class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images')
    image=models.ImageField(upload_to=product_images_directory_path)
    description=models.CharField(max_length=200, null=False, blank=True)


class Order(models.Model):
    class Meta:
        verbose_name = gettext_lazy('Order')
        verbose_name_plural = gettext_lazy('Orders')

    delivery_adress = models.TextField(null=False, blank=True)
    promocode = models.CharField(max_length=20)
    created_at = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    products = models.ManyToManyField(Product, related_name='orders')
    recipt = models.FileField(null=True, upload_to='orders/recipts')

    def __str__(self):
        return f'Adress: {self.delivery_adress}; User: {self.user}; Products: {self.products}'



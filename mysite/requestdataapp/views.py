from django.core.files.storage import FileSystemStorage
from django.http import HttpRequest
from django.shortcuts import render
from .forms import UserBioForm, UploadFileForm

def process_get_view(request:HttpRequest):
    context={

    }
    return render(request,
                  'requestdataapp/request-query-params.html',
                  context=context)

def user_forms(request:HttpRequest):
    context={
        'form': UserBioForm(),
    }
    return render(request,
                  'requestdataapp/user-bio-form.html',
                  context=context)

def handle_file_upload(request:HttpRequest):

    if request.method=='POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            # file=request.FILES['file']
            file=form.cleaned_data['file']
            fs= FileSystemStorage()
            file_name= fs.save(file.name, file)
    else:
        form=UploadFileForm()
    context={
        'form': form
    }
    return render(request, 'requestdataapp/file-upload.html', context=context)
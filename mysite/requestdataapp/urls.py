from django.contrib import admin
from django.urls import path, include

from .views import process_get_view, user_forms,handle_file_upload

app_name='requestdataapp'
urlpatterns = [
    path('', process_get_view, name='process'),
    path('bio/', user_forms, name='bio'),
    path('upload/', handle_file_upload, name='file')
]

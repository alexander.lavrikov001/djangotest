from django import forms
from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import InMemoryUploadedFile
class UserBioForm(forms.Form):
    name=forms.CharField(label='Name', max_length=20)
    age=forms.IntegerField(label='Age', min_value=1, max_value=100)
    bio=forms.CharField(label='Bio', widget=forms.Textarea)

def validate_file_name(file: InMemoryUploadedFile):
    if file.name and 'virus' in file.name:
        raise ValidationError('No should "virus"')


class UploadFileForm(forms.Form):
    file=forms.FileField(validators=[validate_file_name])
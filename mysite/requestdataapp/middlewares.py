from django.http import HttpRequest


def set_useragent_on_request_middleware(get_response):

    def middleware(request:HttpRequest):
        response=get_response(request)
        return response

    return middleware

class CountRequestMiddleware:
    def __init__(self, get_response):
        self.get_response=get_response
        self.counter=0

    def __call__(self, request:HttpRequest):
        self.counter+=1
        respinse=self.get_response(request)
        return respinse
from django.contrib.syndication.views import Feed
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import ListView, DetailView
from rest_framework.reverse import reverse_lazy

from .models import Article

# Create your views here.


class ArticlesListView(ListView):
    queryset = (
        Article.objects.filter(published_at__isnull=False).order_by('-published_at')
    )

class ArticleDetailView(DetailView):
    model = Article

class LatestArticlesFeed(Feed):
    title = 'Блог статей'
    description = 'Обновление и изменение статей'
    link = reverse_lazy('newsapp:articles')

    def items(self):
        return Article.objects.filter(published_at__isnull=False).order_by('-published_at')[:5]

    def item_title(self, item: Article):
        return item.titel

    def item_description(self, item: Article):
        return item.body[:200]

from django.contrib import admin
from django.contrib.syndication.views import Feed
from django.urls import reverse_lazy

from .models import Article


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = 'id', 'titel', 'body', 'published_at'



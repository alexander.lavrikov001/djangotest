import json
from django.test import TestCase
from django.urls import reverse


class GetCookieViewTestCase(TestCase):
    def test_get_cookie_view(self):
        resp=self.client.get(reverse('myauth:cookie-get'))
        self.assertContains(resp, "Cookie value")

class FooBarViewTestCase(TestCase):
    def test_foo_bar_view(self):
        resp= self.client.get(reverse('myauth:foo-bar'))
        self.assertEquals(resp.status_code, 200)
        self.assertEquals(resp.headers['content-type'], 'application/json')
        #
        expected_data={'foo':'bar','spam': 'eggs'}
        #
        # self.assertEquals(json.loads(resp.content),expected_data)
        self.assertJSONEqual(resp.content, expected_data)
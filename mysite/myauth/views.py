from random import random

from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.views import LogoutView
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy, ngettext
from django.views import View
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView, CreateView, UpdateView, DetailView, ListView

from .models import Profile
from .forms import ProfileForms



def login_view(request: HttpRequest):
    if request.method=='GET':
        if request.user.is_authenticated:
            return redirect('/admin/')
        return render(request, 'myauth/login.html')
    username=request.POST['username']
    password = request.POST['password']

    user=authenticate(request, username=username, password=password)
    print(user)
    if user is not None:
        login(request, user)
        return redirect('/admin/')
    return render(request, 'myauth/login.html', {'error': 'Incorrect data'})

def logout_view(request:HttpRequest):
    logout(request)
    return redirect(reverse('myauth:login'))

class MyLogoutView(LogoutView):
    next_page = reverse_lazy('myauth:login')

@user_passes_test(lambda u: u.is_superuser)
def set_cookie_view(request: HttpRequest):
    response=HttpResponse('Cookie set')
    response.set_cookie('fizz', 'buzz', max_age=3600)
    return response

@cache_page(120)
def get_cookie_view(request: HttpRequest):
    value=request.COOKIES.get('fizz', 'default value')
    return HttpResponse(f'Cookie value: {value}+{random()}')




@permission_required('myauth.view_profile', raise_exception=True)
def set_session_view(request: HttpRequest):
    request.session['a']='b'
    return HttpResponse('Session set')

@login_required
def get_session_view(request: HttpRequest):
    value=request.session.get('a', 'default value')
    return HttpResponse(f'Session value: {value}')

class AboutMeView(UpdateView):
    template_name = "myauth/about-me.html"
    model = Profile
    fields = "avatar",'bio'

    success_url = reverse_lazy("myauth:about-me")

    # def form_valid(self, form):
    #     response = super().form_valid(form)
    #     for image in form.files.getlist('images'):
    #         Profile.objects.create(
    #             product=self.object,
    #             image=image
    #         )
    #     return response

    def get_object(self, queryset=None):
        return self.request.user.profile



class RegisterView(CreateView):
    form_class = UserCreationForm
    template_name = 'myauth/register.html'
    success_url = reverse_lazy('myauth:about-me')

    def form_valid(self, form):
        response=super().form_valid(form)
        Profile.objects.create(user=self.object)
        user=authenticate(self.request,username=form.cleaned_data.get('username'),
                          password=form.cleaned_data.get('password1'))
        login(request=self.request,user=user)

        return response

class FooBarView(View):
    def get(self, request: HttpRequest):
        return JsonResponse({'foo': 'bar', 'spam':'eggs'})




class ProfileUpdate(UpdateView):

    model = Profile
    # fields = 'name', 'price', 'description', 'discount', 'preview'
    form_class = ProfileForms
    template_name_suffix = '_update_form'

    def get_object(self, queryset=None):
        return self.request.user.profile

# def get_users(request:HttpRequest):
#     users=Profile.objects.all()
#     return render(request,'myauth/users.html', context={'users': users})

class UserListView(ListView):
    template_name = 'myauth/users.html'
    model=Profile
    context_object_name = 'users'

class UserDetail(DetailView):
    template_name = 'myauth/user-detail.html'
    model = Profile
    context_object_name = 'users'

class UserUpdate(UpdateView):
    def test_func(self):
        return self.request.user.is_superuser

    model = Profile
    form_class = ProfileForms
    template_name = 'myauth/profile_change.html'

    def get_object(self, queryset=None):
        pk = self.kwargs.get(self.pk_url_kwarg)
        print(pk)
        user = Profile.objects.get(pk=pk)
        print(user.user.username)
        return user


class HelloView(View):
    welcome_messge = gettext_lazy('Hello world')
    def get(self, request: HttpRequest):
        items_str=request.GET.get('items') or 0
        items=int(items_str)
        products_line=ngettext(
            'one product',
            '{count} products',
            items
        )
        products_line=products_line.format(count=items)
        return HttpResponse(f'<h1>{self.welcome_messge}</h1>\n<h2>{products_line}</h2>')
